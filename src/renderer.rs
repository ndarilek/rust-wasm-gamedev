use recs::Ecs;
use sdl2::{Sdl, init};
use sdl2::pixels::Color;
use sdl2::render::WindowCanvas;

use input::Controller;

pub struct Renderer(Sdl, WindowCanvas, Controller);

impl Renderer {
    pub fn new() -> Renderer {
        let context = init()
            .expect("Failed to initialize SDL.");
        let video_subsystem = context.video()
            .expect("Failed to initialize SDL video subsystem.");;
        let window = video_subsystem.window("Game", 800, 600)
            .position_centered()
            .fullscreen()
            .opengl()
            .build()
            .expect("Failed to initialize window.");
        let mut canvas = window.into_canvas()
            .build().unwrap();
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        canvas.present();
        let event_pump = context.event_pump().unwrap();
        Renderer(context, canvas, Controller::new(event_pump))
    }

    pub fn tick(&mut self, system: &Ecs) {
        self.2.tick(system);
    }

}
