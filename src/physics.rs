use std::cell::RefCell;
use std::rc::Rc;
use std::time::Instant;

use nphysics2d;
use nphysics2d::object::RigidBody;
use nphysics2d::world::World;

pub struct Physics(pub World<f32>, Option<Instant>);

impl Physics {
    pub fn new() -> Physics {
        let world = nphysics2d::world::World::<f32>::new();
        Physics(world, None)
    }

    pub fn tick(&mut self) {
        if self.1 == None {
            self.1 = Some(Instant::now());
            return;
        }
        let duration = self.1.unwrap().elapsed();
        let delta: f32 = (duration.as_secs() as f32)+(1.0/duration.subsec_nanos() as f32);
        self.0.step(delta);
        self.1 = Some(Instant::now());
    }

}

#[derive(Clone)]
pub struct Body(pub Rc<RefCell<RigidBody<f32>>>);
