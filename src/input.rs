extern crate std;

use recs::{Ecs, EntityId};
use sdl2::EventPump;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use physics::Body;
use tts::Speech;

pub struct Controlled;

pub struct Controller(EventPump);

impl Controller {
    pub fn new(event_pump: EventPump) -> Controller {
        Controller(event_pump)
    }

    pub fn tick(&mut self, system: &Ecs) {
        let mut ids: Vec<EntityId> = Vec::new();
        let filter = component_filter!(Controlled, Speech, Body);
        system.collect_with(&filter, &mut ids);
        for id in ids {
            let speech = system.get::<Speech>(id).unwrap().0;
            let body = system.get::<Body>(id).unwrap().0;
            for event in self.0.poll_iter() {
                match event {
                    Event::KeyDown {keycode: Some(Keycode::C), repeat: false, ..} => {
                        let body = body.borrow();
                        let coordinates = body.position_center();
                        speech.speak(format!("{}, {}", coordinates[0], coordinates[1]).as_str());
                    },
                    Event::KeyDown {keycode: Some(Keycode::H), repeat: false, ..} => {
                        speech.speak("Heading");
                    },
                    _ => {}
                }
            }
        }
    }

}
