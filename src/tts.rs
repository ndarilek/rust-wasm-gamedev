#[cfg(target_os = "emscripten")]

#[derive(Clone, Debug)]
pub struct TTS {
    rate: f32
}

impl TTS {
    pub fn new() -> TTS {
        TTS {
            rate: 1.0
        }
    }

    pub fn set_rate(&mut self, rate: f32) -> &TTS {
        self.rate = rate;
        self
    }

    pub fn speak<S: Into<String>>(&self, text: S) -> &TTS {
        #[cfg(target_os = "emscripten")]
        js!{
            var u = new SpeechSynthesisUtterance();
            u.text = @{text.into()};
            u.rate = @{self.rate};
            speechSynthesis.speak(u);
        };
        self
    }

}

#[derive(Clone)]
pub struct Speech(pub TTS);

impl Speech {
    pub fn new() -> Speech {
        let mut tts = TTS::new();
        tts.set_rate(2.0);
        Speech(tts)
    }

}
