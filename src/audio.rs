use std;
use std::cell::RefCell;
use std::io::Cursor;
use std::sync::Arc;

use alto::{Alto, AltoError, AltoResult, Buffer, Mono, Source, StaticSource, Stereo};
use failure::{Error, err_msg};
use lewton::inside_ogg::OggStreamReader;
use recs::{Ecs, EntityId};

use physics::Body;

pub struct Audio(::alto::Context);

pub struct Listener;

pub struct Emitter(RefCell<StaticSource>);

impl Emitter {
    pub fn relative(&self) -> bool {
        self.0.borrow().relative()
    }

    pub fn set_relative(&mut self, relative: bool) -> &Emitter {
        self.0.borrow_mut().set_relative(relative);
        self
    }

}

impl Audio {
    pub fn new() -> Result<Audio, AltoError> {
        let alto = Alto::load_default()?;
        let device = alto.open(None)?;
        device.new_context(None)
            .map(|v| Audio(v))
    }

    pub fn new_buffer(&self, bytes: &[u8]) -> Result<Arc<Buffer>, Error> {
        let mut reader = OggStreamReader::new(Cursor::new(bytes))
            .map_err(|_| err_msg("Failed to create stream reader"))?;
        let sample_rate = reader.ident_hdr.audio_sample_rate as i32;
        let mut decoded: Vec<i16> = Vec::new();
        while let Some(mut samples) = reader.read_dec_packet_itl()? {
            decoded.append(&mut samples);
        }
        match reader.ident_hdr.audio_channels {
            1 => self.0.new_buffer::<Mono<i16>, _>(decoded, sample_rate),
            2 => self.0.new_buffer::<Stereo<i16>, _>(decoded, sample_rate),
            _ => panic!("Unsupported channel count"),
        }.map(|b| Arc::new(b))
        .map_err(|_| err_msg("Error creating buffer"))
    }

    pub fn new_static_source(&self) -> AltoResult<StaticSource> {
        self.0.new_static_source()
    }

    pub fn new_emitter(&self, b: &Arc<Buffer>) -> Result<Emitter, Error> {
        let mut source = self.new_static_source()?;
        source.set_position([0.0, 0.0, 0.0])?;
        source.set_buffer(b.clone())?;
        source.set_looping(true);
        source.play();
        let emitter = Emitter(RefCell::new(source));
        Ok(emitter)
    }

    pub fn tick(&mut self, system: &mut Ecs) {
        let mut listener_ids: Vec<EntityId> = Vec::new();
        let filter = component_filter!(Listener, Body);
        system.collect_with(&filter, &mut listener_ids);
        for id in listener_ids {
            let body = system.get::<Body>(id).unwrap().0;
            let coordinates = body.borrow().position_center();
            self.0.set_position([coordinates[0], coordinates[1], 0.0]).unwrap();
            self.0.set_orientation(([1.0, 0.0, 0.0], [0.0, 0.0, 1.0])).unwrap();
        }
        let mut emitter_ids: Vec<EntityId> = Vec::new();
        let filter = component_filter!(Emitter, Body);
        system.collect_with(&filter, &mut emitter_ids);
        for id in emitter_ids {
            let emitter = system.borrow::<Emitter>(id).unwrap();
            let body = system.borrow::<Body>(id).unwrap().0.borrow();
            let mut source = emitter.0.borrow_mut();
            if !emitter.relative() {
                let coordinates = body.position_center();
                source.set_position([coordinates[0], coordinates[1], 0.0]).unwrap();
            }
        }
    }

}
