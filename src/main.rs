extern crate alto;
extern crate failure;
extern crate lewton;
extern crate ncollide;
extern crate nphysics2d;
#[macro_use] extern crate recs;
extern crate sdl2;
#[cfg(target_os = "emscripten")]
#[macro_use] extern crate stdweb;

use ncollide::shape::Ball;
use nphysics2d::object::RigidBody;
use recs::Ecs;

mod audio;
#[cfg(target_os = "emscripten")]
mod emscripten;
mod input;
mod physics;
mod renderer;
mod tts;

use audio::Listener;
use input::Controlled;
use physics::Body;
use tts::Speech;

fn start() {
    let mut renderer = renderer::Renderer::new();
    let mut physics = physics::Physics::new();
    let mut audio = audio::Audio::new()
        .expect("Failed to initialize audio.");
    let sfx = vec!(
        include_bytes!("media/sfx/cockpit.ogg")
    );
    let sfx = sfx.iter().map(|b| audio.new_buffer(*b).expect("Failed to load audio.")).collect::<Vec<_>>();
    let mut cockpit = audio.new_emitter(&sfx[0]).expect("Failed to create emitter.");
    //cockpit.set_relative(true);
    let mut system = Ecs::new();
    let player = system.create_entity();
    let _ = system.set(player, Speech::new());
    let _ = system.set(player, Controlled);
    let _ = system.set(player, Listener);
    let rb = RigidBody::new_dynamic(Ball::new(0.5), 1.0, 0.3, 0.6);
    let handle = physics.0.add_rigid_body(rb);
    let _ = system.set(player, Body(handle));
    let game_loop = || {
        physics.tick();
        audio.tick(&mut system);
        renderer.tick(&system);
    };
    #[cfg(target_os = "emscripten")]
    emscripten::set_main_loop_callback(game_loop);
    #[cfg(not(target_os = "emscripten"))]
    loop { game_loop(); };
}

fn main() {
    #[cfg(target_os = "emscripten")]
    stdweb::initialize();
    start();
    #[cfg(target_os = "emscripten")]
    stdweb::event_loop();
}
